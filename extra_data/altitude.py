from __future__ import division 
import csv
import json
import networkx as nx
import matplotlib.pyplot as plt
import pygraphviz as pgv
import geocoder

file = open('data1.csv','rb')
read = csv.reader(file)
outfile = open('data2.csv','wb')
write = csv.writer(outfile)

data = list(read)

#g=geocoder.google('32.68331909,69.41610718')

#print(g.meters)

altitudes = []
i = 0
for line in data:
    g=geocoder.elevation(line[33])
    h=geocoder.google(line[33],method = 'reverse')
    altitudes.append(g.meters)

    i = i + 1
    line.append(g.meters)
    line.append(h.state_long)
    write.writerow(line)

    

print(altitudes)
file.close()
outfile.close()
