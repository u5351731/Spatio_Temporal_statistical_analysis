import pandas as pd
import calverter
from pandas.tseries.holiday import get_calendar, HolidayCalendarFactory, Holiday
import csv
import datetime
import collections


#pandas stuff need to investigate further, seem pretty cool from the look of it.

#Int_cal = get_calendar('AbstractHolidayCalendar')
#Int_cal.rules.append(Holiday('Christmas', month=12, day=25))
#Int_cal.rules.append(Holiday('New Year', month=1, day=1))
#Int_cal.rules.append(Holiday('July 4th', month=7, day=27))

#print (Int_cal.rules)

Islamic_months = ['Throwaway','Muharram', 'Safar', "Rabi' al-awwal", "Rabi' al-thani", 'Jumada al-ula', 'Jumada al-akhirah', 'Rajab', "Sha'ban", 'Ramadan', 'Shawwal', "Dhu al-Qa'dah", 'Dhu al-Hijjah']

file = open('Data.csv')
csv_file = csv.reader(file)

def date_to_day(date):
    day = str(datetime.datetime.strptime(date, '%d/%m/%Y %H:%M').strftime('%A'))
    return day

def remove_time(date):
	date_nt = str(datetime.datetime.strptime(date, '%d/%m/%Y %H:%M').strftime('%d/%m/%Y'))
	date_tuple = tuple(map(int, date_nt.split('/'))) 
	return date_tuple

def is_weekend(day):
    if day == 'Saturday' or day == 'Sunday':
        return True
    else:
        return False

def date_to_islamic(date): #Return String in Islamic date
	cal = calverter.Calverter()
	in_date = remove_time(date)
	jd = cal.gregorian_to_jd(in_date[2], in_date[1], in_date[0])
	islamic = tuple(reversed(cal.jd_to_islamic(jd)))
	islamic_m = Islamic_months[islamic[1]]
	islamic_str = "%s/%s" % (islamic[0], islamic_m)
	return islamic_str

def is_international_holiday(date):
	hdate = (datetime.datetime.strptime(date, '%d/%m/%Y %H:%M').strftime('%d/%m'))
	if hdate == '25/12':
		return "Christmas"
	elif hdate == '01/01':
		return "New Year"
	else:
		return "Not Holiday"

def is_islamic_holiday(date):
	islam_d = date_to_islamic(date)
	if "Ramadan" in islam_d:
		return "Ramadan"
	elif islam_d == '1/Shawwal':
		return "Eid al-Fitr"
	else:
		return "Not Holiday"

print(is_islamic_holiday('20/6/2015 16:30'))
print(is_international_holiday('1/01/2015 16:30'))

#def write_csv(data):
header = ['Date','Day','Weekend','International_Holiday','Islamic_Holiday','Holiday Information']
output = open('Day.csv', 'w')
writer = csv.writer(output, delimiter=',')
writer.writerow(header)

for row in csv_file:
	current = []
	holiday = []
	day = date_to_day(row[1])
	weekend = is_weekend(day)
	current.append(row[1])
	current.append(day)
	current.append(weekend)
	ish = is_islamic_holiday(row[1])
	hd = is_international_holiday(row[1])
	if hd != "Not Holiday":
		current.append(True)
		holiday.append(hd)
	else:
		current.append(False)
	
	if ish != "Not Holiday":
		current.append(True)
		holiday.append(ish)			
	else:
		current.append(False)
	current.append(holiday)	
	writer.writerow(current)
    
    
file.close
#output.close
