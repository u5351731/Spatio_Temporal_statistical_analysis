import csv
import itertools
import dateutil.parser as date_parser

import pandas

headers = (
    '', 'Date', 'Category', 'SubCategory', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '',
    'Latitude', 'Longitude', '', '', '', '', '', '')

csvFile = open('Afghan Kabul War Diary Diaries 2004-2010 - Modified.csv', 'r')
reader = csv.DictReader(csvFile, headers)

# itertools.islice(reader, 100) 
rows = [row for row in reader if row['Latitude'] and row['Longitude']]

for row in rows:
    del row['']  # delete headers we don't care about
    row['Latitude'] = float(row['Latitude'])  # convert floats
    row['Longitude'] = float(row['Longitude'])
    row['Date'] = date_parser.parse(row['Date'])

# 0 = monday, 6 = sunday
dayOfWeekCount = {0: 0, 1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0}

day_of_week_count = dict()

for row in rows:
    weekday = row['Date'].weekday()
    dayOfWeekCount[weekday] += 1

    key = (row['Category'], row['SubCategory'], weekday)

    if key in day_of_week_count:
        day_of_week_count[key] += 1
    else:
        day_of_week_count[key] = 1

day_of_week_freq = dict()
for cat, subcat, weekday in day_of_week_count.keys():
    day_of_week_freq[(cat, subcat, weekday)] = day_of_week_count[(cat, subcat, weekday)] / dayOfWeekCount[weekday]

# aggregate individual days into weekday/weekend
agg_weekday_count = dict()
agg_weekday_freq = dict()  # (cat, subcat) -> (weekdayFreq, weekendFreq)

for (cat, subcat, weekday) in day_of_week_count.keys():

    count = day_of_week_count[(cat, subcat, weekday)]
    freq = day_of_week_freq[(cat, subcat, weekday)]

    is_weekday = weekday < 5

    if (cat, subcat) in agg_weekday_freq:
        weekday_count, weekend_count = agg_weekday_count[(cat, subcat)]
        weekday_freq, weekend_freq = agg_weekday_freq[(cat, subcat)]
        if is_weekday:
            agg_weekday_count[(cat, subcat)] = (weekday_count + count, weekend_count)
            agg_weekday_freq[(cat, subcat)] = (weekday_freq + freq, weekend_freq)
        else:
            agg_weekday_count[(cat, subcat)] = (weekday_count, weekend_count + count)
            agg_weekday_freq[(cat, subcat)] = (weekday_freq, weekend_freq + freq)
    else:
        if is_weekday:
            agg_weekday_count[(cat, subcat)] = (count, 0)
            agg_weekday_freq[(cat, subcat)] = (freq, 0)
        else:
            agg_weekday_count[(cat, subcat)] = (0, count)
            agg_weekday_freq[(cat, subcat)] = (0, freq)

csv_output = []

for cat, subcat in agg_weekday_freq.keys():
    weekday_count, weekend_count = agg_weekday_count[(cat, subcat)]
    weekday_freq, weekend_freq = agg_weekday_freq[(cat, subcat)]

    output = dict()

    output['Category'] = cat
    output['SubCategory'] = subcat
    output['WeekdayCount'] = weekday_count
    output['WeekendCount'] = weekend_count
    output['WeekdayFreq'] = weekday_freq
    output['WeekendFreq'] = weekend_freq

    csv_output.append(output)

out_headers = ['Category', 'SubCategory', 'WeekdayCount', 'WeekendCount', 'WeekdayFreq', 'WeekendFreq']

with open('weekdayend.csv', 'w', encoding='utf8', newline='') as out_file:
    writer = csv.DictWriter(out_file, out_headers)
    writer.writeheader()
    writer.writerows(csv_output)

