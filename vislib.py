# Dump to be read by our CesiumJS Visualisation Engine
from json import JSONEncoder


class VisLibEncoder(JSONEncoder):
    def default(self, o):
        return o.__dict__


class RGB(object):
    def __init__(self, colour):
        self.r = colour.red
        self.g = colour.green
        self.b = colour.blue


class Point(object):
    def __init__(self, long, lat, colour):
        self.lat = lat
        self.long = long
        self.colour = colour  # colour key


class Polygon(object):
    def __init__(self, points, colour):
        self.points = points  # like: [ long1, lat1, long2, lat2, ... ]
        self.colour = colour # colour key


# redundant key of start/end date on poly/points without this.
class VisFrame(object):
    def __init__(self, start, end):
        self.points = []
        self.polygons = []
        self.start_date = start.isoformat()
        self.end_date = end.isoformat()


class VisData(object):
    def __init__(self):
        self.colours = {}  # {name, RGB}
        self.frames = []
