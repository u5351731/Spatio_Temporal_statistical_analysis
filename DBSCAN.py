import pandas as pd
import datetime
import json
from sklearn.cluster import DBSCAN
from colour import Color
from vislib import VisData, VisFrame, Point, VisLibEncoder, Polygon, RGB

df = pd.read_csv(
    'Afghan Kabul War Diary Diaries 2004-2010 - Modified.csv',
    names=['GUID', 'Date', 'Category', 'SubCategory',
           'Unknown1', 'Unknown2', 'Unknown3', 'Unknown4', 'Unknown5', 'Unknown6', 'Unknown7', 'Unknown8',
           'Unknown9', 'Unknown10', 'Unknown11', 'Unknown12', 'Unknown13', 'Unknown14', 'Unknown15', 'Unknown16',
           'Unknown17', 'Unknown18',
           'Latitude', 'Longitude', 'Unknown19', 'Unknown20', 'Unknown21', 'Unknown22', 'Unknown23', 'Unknown24'],
    parse_dates=[1])

# choose which categories will be clustered together, and the categories of the clusters
cluster_schema = {
    'Enemy Action': 'red',
    'Friendly Action': 'blue'
}

# add row num
df['RowNum'] = range(0, len(df))

# filter by category
df = df[df['Category'].isin(cluster_schema.keys())]

# filter out nans
df = df[pd.notnull(df['Latitude'])]
df = df[pd.notnull(df['Longitude'])]

first_date = df['Date'].min()
last_date = df['Date'].max()

frame_num_days = 30

output = VisData()

window_start = first_date
while window_start <= last_date:
    window_end = window_start + datetime.timedelta(days=frame_num_days)

    out_frame = VisFrame(window_start, window_end)

    for cat, colour in cluster_schema.items():
        frame = df[(df['Category'].isin([cat])) & (df['Date'] >= window_start) & (df['Date'] < window_end)]
        if frame.empty:
            window_start = window_start + datetime.timedelta(days=1)
            continue

        frame_coords = frame.as_matrix(columns=['Longitude', 'Latitude'])

        db = DBSCAN(eps=0.2, min_samples=1).fit(frame_coords)
        labels = db.labels_
        num_clusters = len(set(labels)) - (1 if -1 in labels else 0)
        clusters = pd.Series([frame_coords[labels == i] for i in range(num_clusters)])

        for cluster in clusters:
            if len(cluster) > 2:
                points = []
                for coords in cluster:
                    points = points + [coords[0], coords[1]]
                out_frame.polygons.append(Polygon(points, colour))
            # else:
            #     # 2 or less in the cluster, dump 2 points (no polygon).
            #     for coords in cluster:
            #         out_frame.points.append(Point(coords[0], coords[1], colour))

        output.colours[colour] = RGB(Color(colour))

    if out_frame.polygons or out_frame.points:
        output.frames.append(out_frame)

    window_start = window_start + datetime.timedelta(days=1)

json_output = 'var visdata = {0}'.format(json.dumps(output, cls=VisLibEncoder))

f = open('DBSCAN_{0}.js'.format(frame_num_days), 'w')
f.write(json_output)
f.close()

test = 0
