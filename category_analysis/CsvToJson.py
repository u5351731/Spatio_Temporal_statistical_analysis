import csv
import json
import dateutil.parser as date_parser # pip install python-dateutil

headers = (
    '', 'Date', 'Category', 'SubCategory', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '',
    'Latitude', 'Longitude', '', '', '', '', '', '')

csvFile = open('Afghan Kabul War Diary Diaries 2004-2010 - Modified.csv', 'r')
reader = csv.DictReader(csvFile, headers)

rows = [row for row in reader if row['Latitude'] and row['Longitude']]

for row in rows:
    del row['']  # delete headers we don't care about
    row['Latitude'] = float(row['Latitude'])    # convert floats
    row['Longitude'] = float(row['Longitude'])
    row['Date'] = (date_parser.parse(row['Date'])).isoformat() # convert to iso date string

out = json.dumps([row for row in rows], indent=4, skipkeys=True, sort_keys=True)
jsonFile = open('AfghanData.json', 'w')
jsonFile.write(out)
