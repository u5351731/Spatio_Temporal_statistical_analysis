from __future__ import division 
import csv
import json
import networkx as nx
import matplotlib.pyplot as plt
import pygraphviz as pgv


class Events :
    def __init__(self,key,time,category,sub_catagory,place,type_action,danger):
        self.key = key
        self.time = time
        self.category = category
        self.sub_catagory = sub_catagory
        self.place = place
        self.type = type_action
        self.danger = danger
        
class Category :
    def __init__(self,event):
        self.event = event
        self.sub_category = {}
    

file = open('data.csv')
read = csv.reader(file)
data = list(read)

eventslist = []
for line in data:
    if line[2] != "":
        eventslist.append(Events(line[0],line[1],line[2],line[3],line[5],line[6],line[29]))

categories = []
category_list = []
for event in eventslist:
    'print(event.category)'
    if event.category not in category_list:
        category_list.append(event.category)
        categories.append(Category(event.category))


for cat in categories:
    for event in eventslist:    
        if cat.event == event.category:
            cat.sub_category[event.sub_catagory.upper()] = 0
i = 0
for cat in categories:
    total_sub = 0 
    for event in eventslist:   
        if cat.event == event.category:
            cat.sub_category[event.sub_catagory.upper()] += 1
            total_sub += 1
    for sub in cat.sub_category:
        cat.sub_category[sub] = cat.sub_category[sub] / total_sub 


i = 0
j = 0
for cat in categories:
    
    k = 1
    color = []
    G = nx.Graph()
    G.add_node(j,label = cat.event,color = 'red')
    
    'color.append('b')'
    for sub_cat in cat.sub_category:
        'G.add_edge(cat.event,sub_cat)'
        G.add_node(k,label = sub_cat,color = 'blue')
        G.add_edge(j,k,label = str(cat.sub_category[sub_cat]))
        k = k + 1
        color.append('r')
    
    

    A = nx.nx_agraph.to_agraph(G)

    A.layout('dot')
    
    A.draw(cat.event +str(i)+ ".png")      
    'plt.savefig("nodes" +str(i)+ ".png")'
    i = i+1
    '''plt.figure()
    nx.draw(G,label = True,node_color = color)
    plt.savefig("nodes" +str(i)+ ".png")
    i = i+1'''

json_data = {}
for cat in categories:
    json_data[cat.event] = cat.sub_category

json_encoded = json.dumps(json_data)

jsondata = json.dumps(json_data, indent=4, skipkeys=True, sort_keys=True)
fd = open("json_file.txt", 'w')
fd.write(jsondata)
fd.close()
            

            
    

