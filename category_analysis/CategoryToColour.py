import json
from colour import Color

base_colour_names = {
    'Unknown Initiated Action': 'magenta',
    'Criminal Event': 'purple',
    'Counter Insurgency': 'green',
    'Suspicious Incident': 'blue',
    'Other': 'salmon',
    'Explosive Hazard': 'seagreen',
    'Detainee Operations': 'yellow',
    'Counter-Insurgency': 'pink',
    'Air Mission': 'honeydew',
    'Enemy': 'lime',
    'Friendly Fire': 'brown',
    'Friendly Action': 'coral',
    'Enemy Action': 'red',
    'Non-Combat Event': 'crimson'
}


def assignColour(data):
    ret = dict()
    for cat, sub_cats in data.items():
        sub_cats = sub_cats['Subcategories']

        base_colour_name = base_colour_names[cat]
        ret[cat] = dict()

        # normalise to be 0.1 - 0.9 of the lumincance so we don't get too dark/too light
        min_colour = Color(base_colour_name, luminance=0.1)
        max_colour = Color(base_colour_name, luminance=0.9)

        colour_range = list(min_colour.range_to(max_colour, len(sub_cats)))

        # sort sub_cats by frequency
        sorted(sub_cats, key=sub_cats.__getitem__)

        for idx, sub_cat in enumerate(sub_cats.keys()):
            ret[cat][sub_cat] = dict()
            ret[cat][sub_cat]['r'] = colour_range[idx].red
            ret[cat][sub_cat]['g'] = colour_range[idx].green
            ret[cat][sub_cat]['b'] = colour_range[idx].blue
    return ret


file = open('data.json', 'r')
data = json.load(file)
file.close()

colours = assignColour(data)

json_colour = json.dumps(colours, indent=4, skipkeys=True, sort_keys=True)

output = open('colors.json', 'w')
output.write(json_colour)
output.close()
